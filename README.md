# simple-test-package-consoles

## Description

Just a simple package application on how to create a public npm module. This is a barebones/basic npm module on how to publish a npm module and fix up some setup bugs. The `How to use` section is just a description on how to access and use the exported package functions.

## How to use

There are mutiple exported functions that will output different `console.log` messages depending on which function you call.

After installing the module, the package can be accessed through an `import` call:

```
import * as packageLogs from 'simple-test-package-consoles'

// or if not in a js module

const packageLogs = require('simple-test-package-consoles');

```

The following are the exported functions that would provide a static `console-log`:

```
packageLogs.meow();   // meow
packageLogs.woof();   // bark
packageLogs.test();   // This is a test
packageLogs.tracer(); // *creates a long log* TRACER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
```

The next set of functions allows the user to provide their own text to a `console.log` of different types:

```
packageLogs.info("This is an info message");    // INFO:  This is an info message
packageLogs.error("This is an error");          // ERROR: This is an error
packageLogs.custom("This is a custom input");   // This is a custom input
```

Finally, there exists a `hush` variable that when `true` will prevent the console logs from being called:

```
packageLogs.setHush(true);
```

Note: `hush` is set to `false` by default
