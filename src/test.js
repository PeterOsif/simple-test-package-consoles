/* Set the package as a depandency and try some of the functions */
const packageConsoles = require("./index.js");

packageConsoles.setHush(false);

// Do we get our consoles
packageConsoles.meow();
packageConsoles.woof();
packageConsoles.test();
packageConsoles.tracer();

packageConsoles.info("This is an info message");
packageConsoles.error("This is an error");
packageConsoles.custom("This is a custom input");
