/* Simple export functions to just show console logs.  Project is a proof of concept to setup a scoped npm module */

let hush = false;

const error = (errorMessage) => {
  if (!hush) console.error("ERROR: ", errorMessage);
};

const info = (infoMessage) => {
  if (!hush) console.info("INFO: ", infoMessage);
};

const custom = (custom) => {
  if (!hush) console.log(custom);
};

const setHush = (silence) => {
  hush = !!silence;
};

const toggleHush = () => {
  hush = !hush;
};

const tracer = () => {
  if (!hush)
    console.log(
      "TRACER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "
    );
};

const meow = () => {
  if (!hush) console.log("meow");
};

const woof = () => {
  if (!hush) console.log("bark");
};

const test = () => {
  if (!hush) console.log("This is a test");
};

module.exports = {
  error,
  info,
  custom,
  setHush,
  toggleHush,
  tracer,
  meow,
  woof,
  test,
};
